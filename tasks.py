from celery import Celery

BROKER_URL = 'amqp://stardust:admin@192.168.37.90/stardust'
BACKEND_URL = 'redis://192.168.37.90:6379/0'
app = Celery('tasks', backend=BACKEND_URL, broker=BROKER_URL)

@app.task
def add(x, y):
    return x + y